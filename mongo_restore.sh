MONGO_DOCKER_NETWORK=mrt-db-docker_default
MONGO_RESTORE_DB_NAME=ROCKME
MONGO_USERNAME=admin
MONGO_PASSWORD=admin
MONGO_AC_DB=ROCKME

mongorestore -d ${MONGO_RESTORE_DB_NAME} --drop --host mongodb:27017 -u ${MONGO_USERNAME} -p ${MONGO_PASSWORD} --authenticationDatabase ${MONGO_AC_DB} /tmp/ROCKME
echo "drop & restore done"

mongo ${MONGO_RESTORE_DB_NAME} --host mongodb:27017 -u ${MONGO_USERNAME} -p ${MONGO_PASSWORD} \
  --eval 'db.redeem_code.updateMany({"vendor_address" : "0xe4339133f893999a4c6d7a8a02e6257e9fe91eaa"}, {$set: {"vendor_address" : "0xc6e85f4bcf399e74439529c0fbb45d783ae8a4ec"}})' 
mongo ${MONGO_RESTORE_DB_NAME} --host mongodb:27017 -u ${MONGO_USERNAME} -p ${MONGO_PASSWORD} \
  --eval 'db.redeem_code.updateMany({"vendor_address" : "0x5264fc094c09e7a8bbd8a4982f3fe7f0f5a0f0cf"}, {$set: {"vendor_address" : "0x690c44bbb911109f8afaaf0b82c83d7c4ae3524b"}})' 
mongo ${MONGO_RESTORE_DB_NAME} --host mongodb:27017 -u ${MONGO_USERNAME} -p ${MONGO_PASSWORD} \
  --eval 'db.redeem_code.updateMany({"vendor_address" : "0x83127d9051bde2ab6c0a49273fb553dfc172995a"}, {$set: {"vendor_address" : "0x554cdb0fe677a6364324b400152cf3c65e55eea0"}})'
mongo ${MONGO_RESTORE_DB_NAME} --host mongodb:27017 -u ${MONGO_USERNAME} -p ${MONGO_PASSWORD} \
  --eval 'db.redeem_code.updateMany({"vendor_address" : "0xd927cb2053dddbddbc87661b6220587b894ee34d"}, {$set: {"vendor_address" : "0x106823ca00ca6a2a6eaea7d297a1bd856336bbf8"}})'

mongo ${MONGO_RESTORE_DB_NAME} --host mongodb:27017 -u ${MONGO_USERNAME} -p ${MONGO_PASSWORD} \
  --eval 'db.vendor_list.update({"vendor_address" : "0xe4339133f893999a4c6d7a8a02e6257e9fe91eaa"},{$set:{"vendor_address" : "0xc6e85f4bcf399e74439529c0fbb45d783ae8a4ec"}})'
mongo ${MONGO_RESTORE_DB_NAME} --host mongodb:27017 -u ${MONGO_USERNAME} -p ${MONGO_PASSWORD} \
  --eval 'db.vendor_list.update({"vendor_address" : "0x5264fc094c09e7a8bbd8a4982f3fe7f0f5a0f0cf"},{$set:{"vendor_address" : "0x690c44bbb911109f8afaaf0b82c83d7c4ae3524b"}})'
mongo ${MONGO_RESTORE_DB_NAME} --host mongodb:27017 -u ${MONGO_USERNAME} -p ${MONGO_PASSWORD} \
  --eval 'db.vendor_list.update({"vendor_address" : "0x83127d9051bde2ab6c0a49273fb553dfc172995a"},{$set:{"vendor_address" : "0x554cdb0fe677a6364324b400152cf3c65e55eea0"}})'
mongo ${MONGO_RESTORE_DB_NAME} --host mongodb:27017 -u ${MONGO_USERNAME} -p ${MONGO_PASSWORD} \
  --eval 'db.vendor_list.update({"vendor_address" : "0xd927cb2053dddbddbc87661b6220587b894ee34d"},{$set:{"vendor_address" : "0x106823ca00ca6a2a6eaea7d297a1bd856336bbf8"}})'

mongo ${MONGO_RESTORE_DB_NAME} --host mongodb:27017 -u ${MONGO_USERNAME} -p ${MONGO_PASSWORD} \
  --eval 'db.goods.updateMany({"vendor_address" : "0x7fea5ba287e5540d03da5e10683217e784803bcc"},{$set:{"vendor_address" : "0x03d1880b2fec2952a1b0ac1dfccd9a5023c6e0ca"}})'
mongo ${MONGO_RESTORE_DB_NAME} --host mongodb:27017 -u ${MONGO_USERNAME} -p ${MONGO_PASSWORD} \
  --eval 'db.store_list.updateMany({"address" : "0x7fea5ba287e5540d03da5e10683217e784803bcc"},{$set:{"address" : "0x03d1880b2fec2952a1b0ac1dfccd9a5023c6e0ca"}})'
