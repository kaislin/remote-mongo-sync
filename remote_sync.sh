#!/bin/bash
SSH_KEY_PATH=~/rockme.live.pen
SSH_IP=ubuntu@ec2-54-168-90-152.ap-northeast-1.compute.amazonaws.com
SSH_CP_FROM_PATH=/home/ubuntu/mrt-db-docker/backups
SSH_CP_TO_PATH=/tmp
# SSH_CP_FILE_NAME=$(ssh -i ${SSH_KEY_PATH} ${SSH_IP} ls ${SSH_CP_FROM_PATH}/mongo_ROCKME_mongodb_*.gz | tail -n 1)
SSH_CP_FILE_NAME=$(ssh -i ${SSH_KEY_PATH} ${SSH_IP} ls ${SSH_CP_FROM_PATH}/ | tail -n 1)
FILE_NAME=${SSH_CP_FILE_NAME#*backups/}
FILE_NAME_NO_EXTENSION=${FILE_NAME%*.tar*}
PWD=$(pwd)

scp -i ${SSH_KEY_PATH} ${SSH_IP}:${SSH_CP_FROM_PATH}/${SSH_CP_FILE_NAME} ${SSH_CP_TO_PATH}/
tar zxvf ${SSH_CP_TO_PATH}/${FILE_NAME} -C ${SSH_CP_TO_PATH}


MONGO_DOCKER_NETWORK=mrt-db-docker_default
MONGO_RESTORE_DB_NAME=ROCKME
MONGO_USERNAME=admin
MONGO_PASSWORD=admin
MONGO_AC_DB=ROCKME

docker run --rm \
  --network ${MONGO_DOCKER_NETWORK} \
  -v ${SSH_CP_TO_PATH}/${FILE_NAME_NO_EXTENSION}/ROCKME:/tmp/ROCKME \
  -v ${PWD}:/tmp/remote-mongo-sync \
  mongo \
  sh /tmp/remote-mongo-sync/mongo_restore.sh

